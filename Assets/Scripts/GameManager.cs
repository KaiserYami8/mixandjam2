﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    float timeBeforeAction;

    float publicWantGuitar;
    float publicWantBass;
    float publicWantDrum;
    float publicWantMore;
    float publicWantCalm;

    float publicHype;

    public Slider sliderHype;

    int attackChain;

    public Transform selectionCone;

    public enum State
    {
        PlayerTurn,
        PublicTurn,
        Menu
    }
    public State state;

    public GameObject uiPlayer;
    public GameObject uiSelection;

    void Start()
    {

    }

    void Update()
    {
        if (state == State.PublicTurn)
        {
            timeBeforeAction -= Time.deltaTime;

            if (timeBeforeAction <= 0)
            {
                //uiPlayer.SetActive(true);
                uiSelection.SetActive(true);
                state = State.PlayerTurn;
                selectionCone.gameObject.SetActive(true);
            }
        }
        if (state == State.PlayerTurn)
        {
            if (Input.GetButtonDown("Fire2"))
            {
                Back();
            }

            if (EventSystem.current.currentSelectedGameObject.GetComponent<UISnapWorld>() != null)
            {
                selectionCone.position = EventSystem.current.currentSelectedGameObject.GetComponent<UISnapWorld>().target.position;
            }
        }
    }

    public void PlayerAction()
    {
        /*timeBeforeAction = 4f;
        uiPlayer.SetActive(false);
        state = State.PublicTurn;*/
    }

    public void Attack()
    {
        publicHype = 2 + publicWantGuitar;
        publicWantGuitar = 0;

        sliderHype.value = publicHype / 10;

        float shift = FindObjectOfType<RythmController>().Shift();
        if (shift <= .1f)
        {
            attackChain++;
            if (attackChain >= 3)
            {
                timeBeforeAction = 4f;
                uiPlayer.SetActive(false);
                uiSelection.SetActive(false);
                uiSelection.GetComponentInChildren<Button>().Select();
                state = State.PublicTurn;
                attackChain = 0;

                selectionCone.gameObject.SetActive(false);
            }
        }
        FindObjectOfType<RythmController>().pistes.GetChild(2).GetComponent<AudioSource>().mute = false;
    }

    public void Riff()
    {
        publicHype = 3 + publicWantGuitar * 2;
        publicWantGuitar = 0;

        sliderHype.value = publicHype / 10;

        float shift = FindObjectOfType<RythmController>().Shift();
        FindObjectOfType<RythmController>().pistes.GetChild(2).GetComponent<AudioSource>().mute = false;
    }

    public void Wait()
    {
        publicWantGuitar++;

        sliderHype.value = publicHype / 10;
    }

    public void Back()
    {
        uiPlayer.SetActive(false);
        uiSelection.SetActive(true);
        uiSelection.GetComponentInChildren<Button>().Select();
        selectionCone.gameObject.SetActive(true);
    }

    public void SelectPlayer()
    {
        uiSelection.SetActive(false);
        uiPlayer.SetActive(true);
        uiPlayer.GetComponentInChildren<Button>().Select();
    }
}
