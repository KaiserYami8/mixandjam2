﻿using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class UISnapWorld : MonoBehaviour
{
    RectTransform uiElement;

    public Transform target;

    public Vector3 targetOffset;

    private void Start()
    {
        uiElement = GetComponent<RectTransform>();
    }

    void FixedUpdate()
    {
        uiElement.anchoredPosition = Camera.main.WorldToScreenPoint(target.position + targetOffset);
    }
}
