﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;
using DG.Tweening;
using XInputDotNetPure;

public class RythmController : MonoBehaviour
{
    public enum Beat
    {
        One = 1,
        Two = 2,
        Four = 4,
        Eight = 8
    }

    public Slider slider;
    public CinemachineVirtualCamera cineCamera;
    public Camera mainCamera;

    float currentTime;
    float nextBeatTime;
    float previousBeatTime;

    float cameraBump;

    public Beat beat;
    public int bpm = 240;

    public float cameraRotationSpeed = 20;

    public MeshRenderer[] tiles;
    [ColorUsage(true, true)]
    public Color[] colorsAvailable;

    int step;

    bool playSong = false;

    public Transform pistes;

    public RectTransform parent;
    public GameObject handlePref;

    public float offset;

    void Start()
    {
        cameraBump = cineCamera.m_Lens.OrthographicSize;
        nextBeatTime = 1f / (int)beat;

        currentTime += offset;
    }

    private void Update()
    {
        if (Input.GetButtonDown("Fire1") && !playSong)
        {
            playSong = true;

            foreach (Transform piste in pistes)
            {
                //music.Play();
                piste.GetComponent<AudioSource>().PlayScheduled(AudioSettings.dspTime + 60f / bpm * ((int)beat - .5f));
            }
        }

        if (playSong)
        {
            Play();
        }
    }

    void Play()
    {
        currentTime += Time.deltaTime * (bpm / 60f / (int)beat);

        //slider.value = (Mathf.Floor(currentTime * 8) / 8) % 1;
        slider.value = currentTime % 1;

        if (currentTime >= nextBeatTime)
        {
            //Bump Camera
            cineCamera.m_Lens.OrthographicSize = 5;
            DOTween.To(x => cameraBump = x, 5.9f, 6f, .1f);

            previousBeatTime = nextBeatTime;
            nextBeatTime = (Mathf.Floor(currentTime * (int)beat) + 1) / (int)beat;


            float barLenght = 60f / bpm * (int)beat;

            //Tiles Color
            foreach (MeshRenderer tile in tiles)
            {
                tile.material.color = colorsAvailable[Random.Range(0, colorsAvailable.Length)];
                tile.material.SetColor("_EmissiveColor", colorsAvailable[Random.Range(0, colorsAvailable.Length)]);
            }
        }

        cineCamera.m_Lens.OrthographicSize = cameraBump;

        cineCamera.m_Lens.FieldOfView = cameraBump * 10;

        cineCamera.transform.RotateAround(Vector3.zero, Vector3.up, cameraRotationSpeed * Time.deltaTime);

        if (currentTime > step)
        {
            foreach(RectTransform rt in parent)
            {
                Destroy(rt.gameObject);
            }
            step++;
        }


        if (Input.GetButtonDown("Fire1"))
        {
            //Shift();
        }
    }

    public float Shift()
    {
        float shiftBeatTime;
        if (currentTime - previousBeatTime < nextBeatTime - currentTime)
        {
            shiftBeatTime = (currentTime - previousBeatTime) * 60f / bpm * (int)beat;
        }
        else
        {
            shiftBeatTime = (nextBeatTime - currentTime) * 60f / bpm * (int)beat;
        }

        GameObject go = Instantiate(handlePref, parent);
        go.GetComponent<RectTransform>().anchoredPosition = new Vector2(540 * slider.value, 0);

        if (shiftBeatTime <= .05f)
        {
            go.GetComponent<Image>().color = Color.blue;
            StartCoroutine(Rumble(.1f, .1f));
        }
        else if (shiftBeatTime <= .1f)
        {
            go.GetComponent<Image>().color = Color.green;
            StartCoroutine(Rumble(.2f, .1f));
        }
        else
        {
            go.GetComponent<Image>().color = Color.red;
            StartCoroutine(Rumble(1f, .1f));
        }

        return shiftBeatTime;
    }

    IEnumerator Rumble(float strenght, float duration)
    {
        GamePad.SetVibration(0, strenght, strenght);
        yield return new WaitForSeconds(duration);
        GamePad.SetVibration(0, 0f, 0f);
    }
}